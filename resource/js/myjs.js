$(function () {
  $('.modal_trigger .modal_btn').on('click', function() {
    var btnIndex = $(this).index();
    $('.modal_area .modal_box').eq(btnIndex).fadeIn();
  });
  $('.modal_close , .modal_bg').click(function(){
    $('.modal_box').fadeOut();
  });
});


ScrollReveal().reveal('.show', { 
  duration: 1600, 
  origin: 'bottom', 
  distance: '40px',
  delay: 300,
  reset: true 
});





function autoType(elementClass, typingSpeed){
  var thhis = $(elementClass);
  thhis.css({
    "position": "relative",
    "display": "inline-block"
  });
  thhis.prepend('<div class="cursor" style="right: initial; left:0;"></div>');
  thhis = thhis.find(".text-js");
  var text = thhis.text().trim().split('');
  var amntOfChars = text.length;
  var newString = "";
  thhis.text("|");
  setTimeout(function(){
    thhis.css("opacity",1);
    thhis.prev().removeAttr("style");
    thhis.text("");
    for(var i = 0; i < amntOfChars; i++){
      (function(i,char){
        setTimeout(function() {        
          newString += char;
          thhis.text(newString);
        },i*typingSpeed);
      })(i+1,text[i]);
    }
  },500);
}

$(document).ready(function(){
  autoType(".type-js",80);
});



